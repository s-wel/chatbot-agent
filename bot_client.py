from flask import Flask, render_template
from prometheus_flask_exporter import PrometheusMetrics
import mysql.connector
from datetime import date
from mysql.connector import errorcode

# Flask app object
app = Flask(__name__)
metrics = PrometheusMetrics(app)
metrics.info('app_info', 'Application info', version='1.0.0')
# Endpoint
@app.route('/')
def index():
    return render_template('index.html')
    


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8081')
