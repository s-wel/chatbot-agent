import argparse
import requests
import os

# Create Parser object

parser = argparse.ArgumentParser(description="Activate a trained model")
parser.add_argument('model', action='store', help= 'Tag a model as active on production server')
arguments = parser.parse_args()
print(f"Model name is {arguments.model}")

header = {'Content-Type': 'application/json'}
body = {'username': 'me', 'password': os.environ['MODEL_SERVER_CREDENTIAL']}
req = requests.post('http://34.66.84.207/api/auth', json=body, headers=header)
response = req.json()
print("Access token response: ", response)
bearer_token = response["access_token"]
project_id = 'default'
tag = 'production'
model = arguments.model

if bearer_token is not None:
    header = {'Authorization': "Bearer " + bearer_token,
              'Content-Type': 'application/json'}

    print("the URL string : ", f"http://34.66.84.207/api/projects/{project_id}/models/{model}/tags/{tag}", '\n')
    req = requests.put(f"http://34.66.84.207/api/projects/{project_id}/models/{model}/tags/{tag}", headers=header)
    print(req.text, type(req.text), len(req.text))
    if len(req.text) == 0:
        print(str(req.text), "the actual PUT Request")
        print("Model Activated")
else:
    print("Bearer token not retrieved")
